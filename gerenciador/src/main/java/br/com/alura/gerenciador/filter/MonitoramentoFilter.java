package br.com.alura.gerenciador.filter;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

@WebFilter(filterName = "MF", urlPatterns = "/entrada")
public class MonitoramentoFilter implements Filter {
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		System.out.println("MonitoramentoFilter");
		Instant start = Instant.now();
		chain.doFilter(request, response);
		Instant finish = Instant.now();
		Duration timeElapsed = Duration.between(start, finish);
		System.out.println("Tempo de execu��o: " + timeElapsed.toMillis() + " milisegundos.");
	}

}
