package br.com.alura.gerenciador.modelo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class Banco {

	private static List<Empresa> empresas = new ArrayList<Empresa>();
	private static List<Usuario> usuarios = new ArrayList<Usuario>();
	private static Integer chaveSequencial = 1;

	static {
		gerarEmpresa("Alura");
		gerarEmpresa("Caelum");
		gerarEmpresa("Hospital do cora��o");

		gerarUsuario("nallon", "123");
		gerarUsuario("nillo", "123");
	}

	public void adiciona(Empresa empresa) {
		empresa.setId(chaveSequencial++);
		empresas.add(empresa);
	}

	public void remove(Integer id) {
		empresas.removeIf(empresa -> empresa.getId() == id);

//		Iterator<Empresa> it = empresas.iterator();
//		while (it.hasNext()) {
//			Empresa emp = it.next();
//
//			if (emp.getId() == id) {
//				it.remove();
//			}
//		}
	}

	public Empresa buscaEmpresaPeloId(Integer id) {
		Optional<Empresa> empresa = empresas.stream()
				.filter(e -> e.getId()
						.equals(id))
				.findFirst();
		return empresa.get();
	}

	public List<Empresa> getEmpresas() {
		return empresas;
	}

	public Usuario existeUsuario(String login, String senha) {
		Optional<Usuario> usuario = usuarios.stream()
				.filter(u -> u.ehIgual(login, senha))
				.findFirst();
		return usuario.get();
	}

	private static void gerarEmpresa(String empresa) {
		Empresa hospital = new Empresa();
		hospital.setId(chaveSequencial++);
		hospital.setNome(empresa);
		hospital.setDataAbertura(new Date());
		empresas.add(hospital);
	}

	private static void gerarUsuario(String login, String senha) {
		Usuario u1 = new Usuario();
		u1.setLogin(login);
		u1.setSenha(senha);
		usuarios.add(u1);
	}
}
