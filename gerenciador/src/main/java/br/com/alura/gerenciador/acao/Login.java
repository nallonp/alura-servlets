package br.com.alura.gerenciador.acao;

import java.io.IOException;
import java.util.NoSuchElementException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.alura.gerenciador.modelo.Banco;
import br.com.alura.gerenciador.modelo.Usuario;

public class Login implements Acao {

	@Override
	public String executa(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String login = request.getParameter("login");
		String senha = request.getParameter("senha");
		Banco banco = new Banco();
		try {
			Usuario usuario = banco.existeUsuario(login, senha);
			HttpSession session = request.getSession();
			session.setAttribute("usuarioLogado", usuario);
		} catch (NoSuchElementException e) {
			return "redirect:entrada?acao=LoginForm";
		}
		return "redirect:entrada?acao=ListaEmpresas";
	}

}
