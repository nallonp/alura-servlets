package br.com.alura.gerenciador.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class OiMundoServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5740109949109320353L;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		out.println("<html>");
		out.println("<body>");
		out.println("<p>Oi mundo!!!</p>");
		out.println("</body>");
		out.println("</html>");
		System.out.println("O servlet OiMundoServlet foi chamado!");
	}
}
