<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<c:import url="logout-parcial.jsp" />
	<form action=<c:url value="/entrada" /> method="post">
		<input type="hidden" id="id" name="id" value="${ empresa.id }">
		<label for="nome">Nome:</label> <input type="text" id="nome"
			name="nome" value="${ empresa.nome }"> <label for="nome">Data
			abertura:</label> <input type="text" id="data" name="data"
			value="<fmt:formatDate	value="${ empresa.dataAbertura }" pattern="dd/MM/yyyy" />">
		<input type="hidden" name="acao" value="AlteraEmpresa"> <input
			type="submit" value="Submit">
	</form>
</body>
</html>