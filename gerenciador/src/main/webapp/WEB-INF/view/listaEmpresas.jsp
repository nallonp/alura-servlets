<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List,br.com.alura.gerenciador.modelo.Empresa"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<c:import url="logout-parcial.jsp" />

	<c:if test="${not empty empresa }">
		<p>Empresa ${ empresa } cadastrada com sucesso!</p>
	</c:if>
	<h1>Lista de empresas</h1>
	<ul>
		<c:forEach items="${ empresas }" var="empresa">
			<li>${empresa.id }-${ empresa.nome }-<fmt:formatDate
					value="${ empresa.dataAbertura }" pattern="dd/MM/yyyy" /> <a
				href=<c:url value="/entrada?acao=MostraEmpresa&id=${ empresa.id }"/>>editar</a>
				<a
				href=<c:url value="/entrada?acao=RemoveEmpresa&id=${ empresa.id }"/>>remover</a>
			</li>
		</c:forEach>
	</ul>
	<p>Usu�rio logado: ${ usuarioLogado.login }</p>

	<%-- 	<ul> 
		List
		<Empresa> empresas = (List<Empresa>)
		request.getAttribute("empresas"); for (Empresa empresa : empresas) {
		<li><%=empresa.getNome()%></li>
		} %> 
	</ul> --%>
</body>
</html>