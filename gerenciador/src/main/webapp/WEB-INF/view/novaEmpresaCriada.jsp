<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<body>
	<c:import url="logout-parcial.jsp" />
	<c:if test="${not empty empresa }">
		<p>Empresa ${ empresa } cadastrada com sucesso!</p>
	</c:if>
	<c:if test="${empty empresa }">
		<p>??????????? Voc� chegou nessa p�gina por um caminho inv�lido.</p>
	</c:if>
	<%-- <p>Empresa <%= nomeEmpresa %> cadastrada com sucesso!</p> --%>
</body>
</html>